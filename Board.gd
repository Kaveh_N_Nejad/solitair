extends Node2D

var CardScene = preload("res://Card.tscn")

var cardData = {}
var cardIndex = {}
onready var decks = [$Decks/Deck1,$Decks/Deck2,$Decks/Deck3,$Decks/Deck4,$Decks/Deck5,$Decks/Deck6,$Decks/Deck7]
var cardDataDublicate

func _ready():
	getJsonData()
	setUpDecks()
	$Win.disabled = true
		
func setUpDecks():
	cardDataDublicate = cardData.duplicate()
	randomize()
	setUpBottomDecks()
	setUpMainDeck()
	
func setUpMainDeck():
	for _i in range(cardDataDublicate.keys().size()):
		var newCard = CardScene.instance()
		newCard.setup(getCardFromDict(0))
		$MainDeck.addCard(newCard)
		add_child(newCard)
	$MainDeck/DeckLeft.cards.shuffle()
		
func setUpBottomDecks():
	for deck in range(7):
		for _card in range(deck + 1):
			var newCard = CardScene.instance()
			var randomIndex = randi() % int(cardDataDublicate.keys().size())
			newCard.setup(getCardFromDict(randomIndex))
			decks[deck].addCard(newCard)
			add_child(newCard)

	for deck in decks:
		deck.cards.back().flip()
		
func getCardFromDict(index):
	var card = cardDataDublicate[cardDataDublicate.keys()[index]]
	cardDataDublicate.erase(cardDataDublicate.keys()[index])
	return card

func getJsonData():
	var file = File.new()
	file.open("res://cards.json", file.READ)
	var text = file.get_as_text()
	cardData = parse_json(text)
	file.close()
	
func win():
	$Reshuffle.disabled = true
	clear()
	waves()

func waves():
	cardDataDublicate = cardData.duplicate()
	for i in range(cardDataDublicate.keys().size() - 1):
		var newCard = CardScene.instance()
		newCard.setup(cardDataDublicate[cardDataDublicate.keys()[i]])
		newCard.flip()
		$Path.spawn(newCard)
		yield(get_tree().create_timer(0.5),"timeout")

func clear():
	for deck in decks:
		deck.deleteAllCards()
	$"4Final".deleteAllCards();
	$MainDeck/DeckLeft.deleteAllCards()
	$MainDeck/DeckRight.deleteAllCards()
	
func _on_Button_pressed():
	clear()
	setUpDecks()
