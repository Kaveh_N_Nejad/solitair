extends Area2D

var cards = []

var typeOfDeck = "Simple"

func addCard(newCard):
	if typeOfDeck != "4Main":
		addCardToNonMain(newCard)
	elif typeOfDeck == "4Main":
		addCardToMain(newCard)
	newCard.deck = self
	cards.append(newCard)
	newCard.z_index = cards.size()

func addCardToNonMain(newCard):
	if !cards:
		newCard.position = position
	else:
		newCard.position = cards.back().position + Vector2(0,30)

func addCardToMain(newCard):
	newCard.position = position + get_parent().position
	if cards:
		cards.back().switchColisionState()
	if newCard.number == 13:
		get_parent().checkVictory()

func removeCard():
	cards.pop_back()
	if typeOfDeck == "Main" || typeOfDeck == "4Main":
		if cards:
			cards.back().switchColisionState()
	elif cards:
		if !cards.back().isVisible:
			cards.back().flip()

func removeAllCards():
	cards = []

func canDropCard(card):
	if typeOfDeck == "Simple":
		if cards.size() != 0:
			var topCard = cards.back()
			return (topCard.number - 1 == card.number
			&& topCard.color != card.color)	
		else:
			return card.number == 13
	elif typeOfDeck == "4Main":
		if cards.size() == 0:
			return card.number == 1
		else:
			return card.suit == cards[0].suit && card.number == cards.back().number + 1
	return false

func deleteAllCards():
	for card in cards:
		card.queue_free()
		cards = []
