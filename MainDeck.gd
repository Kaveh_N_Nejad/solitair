extends Node2D

func _ready():
	$DeckLeft/CollisionShape2D.disabled = true
	$DeckRight/CollisionShape2D.disabled = true
	$DeckLeft.typeOfDeck = "Main"
	$DeckRight.typeOfDeck = "Main"

func addCard(card):
	card.deck = self
	$DeckLeft.addCard(card)
	card.position = position + $DeckLeft.position
	card.switchColisionState()

func _on_DeckLeft_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton && event.is_pressed():
		if $DeckLeft.cards:
			revial()
		else:
			reset()

func addCardToRight(card):
	card.flip()
	if $DeckRight.cards:
		$DeckRight.cards.back().switchColisionState()
	$DeckRight.addCard(card)
	card.position = position + $DeckRight.position
	card.switchColisionState()

func revial():
	addCardToRight($DeckLeft.cards.pop_back())
		
func reset():
	$DeckRight.cards.back().switchColisionState()
	$DeckRight.cards.invert()
	for card in $DeckRight.cards:
		card.flip()
		card.z_index = $DeckLeft.cards.size()
		$DeckLeft.addCard(card)
		card.position = position + $DeckLeft.position
		
	$DeckRight.removeAllCards()
