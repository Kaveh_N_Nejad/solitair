extends Node2D

onready var decks = [$Deck,$Deck2,$Deck3,$Deck4]

func _ready():
	for deck in decks:
		deck.typeOfDeck = "4Main"

func checkVictory():
	var won = true
	for deck in decks:
		if deck.cards.size() != 13:
			won = false
	if won:
		get_parent().win()

func deleteAllCards():
	for deck in decks:
		deck.deleteAllCards()
