extends Path2D

onready var fallingCard = preload("res://PathFollow2D.tscn")

var cards = []

func _process(delta):
	cards = get_children()
	for card in cards:
		card.set_offset(card.get_offset() + 200 * delta)

func spawn(newCard):
	var card = fallingCard.instance()
	card.start(newCard)
	add_child(card)
