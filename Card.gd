extends Area2D

var number
var suit
var color

var deck
var draging = false
var dragingMultiple = false
var lastPosition
var lastZIndex
var followingCards = []
var isVisible = false
var offset

func _process(_delta):
	if draging:
		dragCard()

func setup(dict):
	number = dict["Number"]
	suit =  String(dict["Type"])
	color = dict["Color"]
	$Cover.texture = load(dict["BackGround"])
	$Sprite.texture = load(dict["ForeGround"])

func flip():
	$Cover.visible = !$Cover.visible
	isVisible = !$Cover.visible

func _on_Card_input_event(_viewport, event, _shape_idx):
	checkIfDragable(event)

func checkIfDragable(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if isCardsLast():
				startDraging()
			elif isGrabingTop(event) && !$Cover.visible:
				startDragingMultiple()
		else:
			if draging || dragingMultiple:
				dropped()

func startDraging():
	draging = true
	offset = position - get_viewport().get_mouse_position()
	lastPosition = position
	lastZIndex = z_index
	z_index = 100

func startDragingMultiple():
	setFollowingCards()
	startDraging()
	dragingMultiple = true
	
func setFollowingCards():
	for i in range(cardPositionInDeck() + 1, deck.cards.size(), 1):
		followingCards.append(deck.cards[i])
	for i in range(followingCards.size()):
		var card = followingCards[i]
		card.lastPosition = card.position
		card.lastZIndex = card.z_index
		card.z_index = 101 + i

func getDeckOfArea():
	var newDeck = null
	for area in get_overlapping_areas():
		if area.is_in_group("Card"):
			if (area.deck.canDropCard(self)):
				newDeck = area.deck
		elif area.is_in_group("Deck"):
			if area.canDropCard(self):
				newDeck = area
	return newDeck
	
func dropped():
	var newDeck = getDeckOfArea()
	if newDeck:
		dropCard(newDeck)
		for card in followingCards:
			card.dropCard(newDeck)
	else:
		replaceCards()
		
	dragingMultiple = false
	draging = false
	followingCards = []

func replaceCards():
		position = lastPosition
		z_index = lastZIndex
		if dragingMultiple:
			for card in followingCards:
				card.z_index = card.lastZIndex
				card.position = card.lastPosition

func switchColisionState():
	$CollisionShape2D.disabled = !$CollisionShape2D.disabled

func isCardsLast():
	return deck.cards.size() - 1 == cardPositionInDeck()
	
func isGrabingTop(event):
	return event.position.y < position.y - 30
	
func cardPositionInDeck():
	return deck.cards.find(self)

func dragCard():
	position = get_viewport().get_mouse_position() + offset
	if dragingMultiple:
		dragMultiple()

func dragMultiple():
	for i in range(followingCards.size()):
		followingCards[i].position = position + Vector2(0,(i + 1) * 30)

func dropCard(newDeck):
	deck.removeCard()
	newDeck.addCard(self)
